## Dataset tools repository 
![image](https://drive.google.com/uc?export=view&id=1mUtWCsD5ec7otPhl_QTc0-_aBPcMGceK)

This is a tools repository to assist in a data pipeline for the creation of the Waterloo Multi Agent Traffic dataset. At this point, for intersection_dataset and crosswalk_dataset, the code and table creation is scattered in individual repositories. Over time the tools code will be moved here.

If you are just using the WMA dataset to build models, there should not be any need to clone this project.
