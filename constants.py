'''
Created on Nov 19, 2021

@author: Atrisha
'''
import numpy as np

ALL_FILE_IDS = [x for x in list(np.arange(917,935)) if x != 928] + list(np.arange(988,997))
DB_PATH_HOME = 'D:\\roundabout_dataset'