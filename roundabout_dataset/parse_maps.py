'''
Created on Oct 19, 2021

@author: Atrisha
'''
import csv
from collections import OrderedDict
import numpy as np
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
import ast
from pyproj import Proj
from shapely.geometry import LineString, Polygon
import matplotlib
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from shapely.geometry import Polygon as shapelyPolygon


def parse_trajectories():
    trajectories = OrderedDict()
    ''' This is just an example to check if the trajectories align well with the map'''
    file_path = "D:\\roundabout_dataset_processed\\wetransfer-3a3870\\exported\\trajectories_917.csv"
    with open(file_path, newline='') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=';', skipinitialspace=True)
        ct = 0
        for row in csvreader:
            size = len(row)
            if ct > 0:
                ins_list = [None if x.strip()=='-' else x for x in row[:8]]
                ins_list = [x.strip("'") if x is not None else None for x in ins_list]
                track_id = ins_list[0]
                if track_id not in trajectories:
                    trajectories[track_id] = []
                i = 0
                print('processing',track_id)
                rest = row[8:]
                while i+8 < size: 
                    ins_traj_list = rest[i:i+8]
                    if len(ins_traj_list) == 8:
                        ins_traj_list = [None if x.strip()=='-' else x for x in ins_traj_list]
                        ins_traj_list = [x.strip('"') if x is not None else None for x in ins_traj_list]
                        traj_ts = ins_traj_list[5]
                        trajectories[track_id].append(ins_traj_list) 
                    i += 8
            ct += 1
    #samples = np.random.choice(a = list(trajectories.keys()), size =200)
    fig, ax = plt.subplots()
    samples = list(trajectories.keys())
    for k,v in trajectories.items():
        if k in samples:
            _x,_y = [float(v[idx][0]) for idx in np.arange(0,len(v),5)], [float(v[idx][1]) for idx in np.arange(0,len(v),5)]
            #ax.plot(_x,_y,'-',c='b',alpha=0.2)
            #print(k,len(v))
            #print([(v[idx][0],v[idx][1],v[idx][5]) for idx in np.arange(0,len(v),5) ])
    patches = parse_roundabout_map(ax)
    p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=0.4)
    colors = 100*np.random.rand(len(patches))
    p.set_array(np.array(colors))
    ax.add_collection(p)
    plt.axis('equal')
    plt.show()
    
def parse_roundabout_map(ax):
    patches = []
    way_map, lane_map = dict(), dict()
    #myProj = Proj("+proj=utm +zone=18T, +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
    #root = ET.parse('D:\\repeated_games_data\\intersection_dataset\\maps\\nyc_lanes.osm').getroot()
    myProj = Proj("+proj=utm +zone=17T, +north +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
    root = ET.parse('..\\mapfiles\\roundabout_ll3.osm').getroot()
    node_map = dict()
    for type_tag in root.findall('node'):
        if type_tag.attrib['id'] not in node_map:
            node_map[type_tag.attrib['id']] = (type_tag.attrib['lat'],type_tag.attrib['lon'])
        '''
        node_tags = type_tag.findall('tag')
        if len(node_tags) > 0:
            lat,lon = float(type_tag.attrib['lat']), float(type_tag.attrib['lon']) 
            utm_proj = myProj(lon, lat)
            if node_tags[0].attrib['k'] == 'name':
                print(node_tags[0].attrib['v'],utm_proj)
        '''
    for type_tag in root.findall('way'):
        if type_tag.find('tag') is not None:
            for way_id in type_tag.find('tag').attrib['v'].split(','):
                #way_id = type_tag.attrib['id']
                #print('----',lane_tag)
                for nd in type_tag:
                    if 'ref' in nd.attrib:
                        lat,lon = float(node_map[nd.attrib['ref']][0]), float(node_map[nd.attrib['ref']][1]) 
                        utm_proj = myProj(lon, lat)
                        
                        #print(utm_proj)
                        if way_id not in way_map:
                            way_map[way_id] = [utm_proj]
                        else:
                            way_map[way_id].append(utm_proj)
    for k,v in way_map.items():
        way_region, way_orientation = k.split('_')[:-1],k.split('_')[-1]
        way_other_orientation = 'left' if way_orientation == 'right' else 'right'
        other_lane_tag = '_'.join(way_region+[way_other_orientation])
        if other_lane_tag in way_map:
            if '_'.join(way_region) not in lane_map:
                or1, or2 = way_orientation+'_boundary', way_other_orientation+'_boundary', 
                lane_map['_'.join(way_region)] = {or1:v, or2:way_map['_'.join(way_region+[way_other_orientation])]}
        else:
            print(k)
        #print(k,v)
        #plt.plot([float(x[0]) for x in  v],[float(x[1]) for x in  v],'--',c='black')
    for k,v in lane_map.items():
        print(k)
        for k1,v1 in v.items():
            print('\t',k1,v1)
        lane_region = np.array([list(x) for x in v['left_boundary']+v['right_boundary'][::-1]])
        pl = Polygon(xy=lane_region, closed = False)   
        centr = shapelyPolygon([list(x) for x in v['left_boundary']+v['right_boundary'][::-1]]).centroid
        if ax is not None:
            ax.annotate(k, (centr.x, centr.y), color='black', fontsize=6, ha='center', va='center')
        patches.append(pl)
    return lane_map

if __name__ == '__main__':
    #parse_trajectories()
    parse_roundabout_map(None)