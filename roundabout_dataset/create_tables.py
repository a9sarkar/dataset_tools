'''
Created on Nov 19, 2021

@author: Atrisha
'''
import sqlite3
import constants as tools_constants
import os
from roundabout_dataset.parse_maps import parse_roundabout_map
from shapely.geometry import Polygon, Point
import numpy as np
import matplotlib.pyplot as plt
import csv

def distance(data):
    return np.sum((data[1:] - data[:-1]) ** 2, axis=1) ** .5

def draw_path(path):
    path = np.asarray(path)
    HEAD_WIDTH = 2
    HEAD_LEN = 3

    fig = plt.figure()
    axes = fig.add_subplot(111)

    x = path[:,0]
    y = path[:,1]
    axes.plot(x, y)

    theta = np.arctan2(y[1:] - y[:-1], x[1:] - x[:-1])
    dist = distance(path) - HEAD_LEN

    x = x[:-1]
    y = y[:-1]
    ax = x + dist * np.sin(theta)
    ay = y + dist * np.cos(theta)

    for x1, y1, x2, y2 in zip(x,y,ax-x,ay-y):
        axes.arrow(x1, y1, x2, y2, head_width=HEAD_WIDTH, head_length=HEAD_LEN)
    plt.show()

def create_traffic_regions_def():
    for file_id in [str(x) for x in tools_constants.ALL_FILE_IDS]:
        conn = sqlite3.connect(os.path.join(tools_constants.DB_PATH_HOME,file_id+'.db'))
        c = conn.cursor()
        create_statement = 'CREATE TABLE "TRAFFIC_REGIONS_DEF" ( `NAME` TEXT, `REGION_ID` INTEGER, `SHAPE` TEXT, `REGION_PROPERTY` TEXT, `X_POSITIONS` TEXT, `Y_POSITIONS` TEXT )'
        c.execute(create_statement)
        conn.commit()

def insert_traffic_regions_def():
    regions = parse_roundabout_map(None)
    ins_list = []
    region_id = 0
    for region_name, region_info in regions.items():
        region_id += 1
        for boundary_type, boundary_info in region_info.items():
            boundary_x = [x[0] for x in boundary_info]
            boundary_y = [x[1] for x in boundary_info]
            region_property = 'lane_'+boundary_type
            ins_list.append((region_name,region_id,'polyline',region_property,str(boundary_x),str(boundary_y)))
    ins_statement = 'INSERT INTO TRAFFIC_REGIONS_DEF VALUES (?,?,?,?,?,?)'
    for file_id in [str(x) for x in tools_constants.ALL_FILE_IDS]:
        conn = sqlite3.connect(os.path.join(tools_constants.DB_PATH_HOME,file_id+'.db'))
        c = conn.cursor()
        c.executemany(ins_statement,ins_list)
        conn.commit()
        conn.close()
    f=1

def load_traffic_regions():
    regions = parse_roundabout_map(None)
    traffic_regions = dict()
    region_id = 0
    for region_name, region_info in regions.items():
        region_id += 1
        left_boundary, right_boundary =  region_info['left_boundary'], region_info['right_boundary']
        region_polygon = Polygon(left_boundary+right_boundary[::-1])
        traffic_regions[region_name] = region_polygon
    return traffic_regions   

def assign_traffic_segments():
    ''' This function assign the traffic segments to the TRAJECTORIES table '''
    traffic_regions = load_traffic_regions()
    
    for file_id in [str(x) for x in tools_constants.ALL_FILE_IDS]:
        ins_list = []
        ins_string = "INSERT INTO TRAJECTORIES_0"+file_id+" VALUES (?,?,?,?,?,?,?,?,?,?)"
        conn = sqlite3.connect(os.path.join(tools_constants.DB_PATH_HOME,file_id+'.db'))
        c = conn.cursor()
        q_string = 'select * from TRAJECTORIES_0'+file_id
        c.execute(q_string)
        res = c.fetchall()
        for row in res:
            track_pt = Point(row[2], row[3])
            assigned_regions = ','.join([reg for reg,p in traffic_regions.items() if p.contains(track_pt)])
            new_row = tuple(list(row)[:-1] + [assigned_regions])
            ins_list.append(new_row)
            print('file:'+file_id,'index:'+str(row[0]))
        c.execute("DELETE FROM TRAJECTORIES_0"+file_id)
        conn.commit()
        c.executemany(ins_string,ins_list)
        conn.commit()
        conn.close()    

def assign_segment_seq():
    ''' This function assigns the TRAFFIC_SEGMENT_SEQ values to TRAJECTORIES_MOVEMENTS table
        Dependencies: assign_traffic_segments should have already been run prior to execution '''
    
    for file_id in [str(x) for x in tools_constants.ALL_FILE_IDS]:
        segment_seq_map = dict()
        print('file:'+file_id)
        up_list = []
        conn = sqlite3.connect(os.path.join(tools_constants.DB_PATH_HOME,file_id+'.db'))
        c = conn.cursor()
        q_string = "SELECT TRAFFIC_REGIONS, TRACK_ID, TIME FROM TRAJECTORIES_0"+file_id+" GROUP BY TRACK_ID, TRAFFIC_REGIONS ORDER BY TRACK_ID, TIME"
        c.execute(q_string)
        res = c.fetchall()
        for row in res:
            if row[0] != '':
                if row[1] not in segment_seq_map:
                    segment_seq_map[row[1]] = [row[0]]
                else:
                    segment_seq_map[row[1]].append(row[0])
        u_string = "UPDATE TRAJECTORY_MOVEMENTS SET TRAFFIC_SEGMENT_SEQ =? where TRACK_ID=?"
        for track_id,segment_seq in segment_seq_map.items():
            up_list.append((','.join(segment_seq), track_id))
        c.executemany(u_string,up_list)
        conn.commit()
        conn.close()
        f=1
        

def add_lane_gate_tags():
    ''' This simply adds the missing lane gate tags. 
        CAUTION: DO NOT RUN IF SEGMENT GATES HAVE ALREADY BEEN ADDED TO GATE_CROSSING_EVENTS 
        In fact, DO NOT run this ever. I have commented the execute statement.  '''
    
    for file_id in [str(x) for x in tools_constants.ALL_FILE_IDS]:
        print('file:'+file_id)
        conn = sqlite3.connect(os.path.join(tools_constants.DB_PATH_HOME,file_id+'.db'))
        c = conn.cursor()
        u_string = "UPDATE GATE_CROSSING_EVENTS SET GATE_TYPE ='LANE_GATE'"
        #c.execute(u_string)
        conn.commit()
        conn.close()

def add_lane_relations():
    ''' THis function is to create the lane relations just how one would in a lanelet2 format.
        However, given that this is only one location, we created it manually. In future, this
        can be transformed to lanelet2, especially since we already have the osm available for this location.'''
    file_path = '..\\mapfiles\\next_seg_list.csv'
    ins_list = []
    with open(file_path, newline='') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',', skipinitialspace=True)
        ct = 0
        for row in csvreader:
            if row[2] !='':
                ins_list.append((row[0],row[1],'NEXT_SEGMENT'))
                ins_list.append((row[0],row[2],'NEXT_SEGMENT'))
            else:
                if row[1] !='':
                    ins_list.append((row[0],row[1],'NEXT_SEGMENT'))
                else:
                    ins_list.append((row[0],None,'NEXT_SEGMENT'))
    create_str = "CREATE TABLE `LANE_RELATIONS` ( `LANE_1` TEXT, `LANE_2` TEXT, `RELATION_TYPE` TEXT )"
    ins_statement = "INSERT INTO LANE_RELATIONS VALUES (?,?,?)"
    for file_id in [str(x) for x in tools_constants.ALL_FILE_IDS]:
        print('file:'+file_id)  
        conn = sqlite3.connect(os.path.join(tools_constants.DB_PATH_HOME,file_id+'.db'))
        c = conn.cursor() 
        c.execute(create_str)
        conn.commit() 
        c = conn.cursor()
        c.executemany(ins_statement,ins_list)
        conn.commit()
        conn.close()
                    
    
if __name__ == '__main__':
    #insert_traffic_regions_def()
    add_lane_relations()